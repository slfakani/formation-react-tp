import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { constants } from '../constants';

export default function DetailsPage() {
  const { id } = useParams();
  
  useEffect(() => {
    // TODO: Appel à l'API pour récupérer les données de l'hôtel
  }, [id]);

  return (
    <div className="container m-3">
      {/* TODO: affichage des détails */}
    </div>
  );
}
