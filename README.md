# Initiation React - TP

# Initiation Web : TP React
## Etape 1 : La mise en place de l'API
Dans un autre dossier, clonez le dépôt de l'API avec la commande suivante :
```
git clone --branch new-routes https://gitlab.isima.fr/nimurillon/initiationapi.git
```

Installez les dépendances avec la commande

```
pip install fastAPI[all]
```

Créez la base de données avec la commande : 
```
python3 load_db.py
```

Lancez l'API avec la commande suivante, puis laissez-la tourner : 
```
python3 -m uvicorn main:app --reload
```

C'est tout bon ! 
Si vous ne comprenez pas le code de l'API, ce n'est pas grave, on verra ça bientôt...

## Etape 2 : Mise en place du projet 

Installez les dépendances du projet avec la commande ``npm install`` puis utilisez ``npm start`` pour lancer le serveur de développement. Visitez ``https://localhost:3000``, vous devriez voir votre page.
Votre but est d'arriver à afficher une liste d'hôtels, composées de cartes où vous pouvez accéder aux détails de chaque hôtel, puis en ajouter un.

## Etape 3 : La carte d'un hôtel

Dans le fichier ``HotelCard.jsx`` du dossier ``components``, modifiez le retour de la fontion pour afficher le nom de l'hôtel, sa description et un bouton pour accéder aux détails (sur /hotel/id).

## Etape 4 : La liste des hôtels

Dans le fichier ``HomePage.jsx``, utilisez les hooks ``useState`` et ``useEffect`` pour réaliser un appel à l'API au chargement du composant.
Affichez dans le retour de la fonction les cartes en utilisant le composant ``HotelCard``.

## Etape 5 : Les détails d'un hôtel

Dans ``DetailsPage.jsx``, faites un appel à l'API pour fetcher l'hôtel et affichez ses détails dans le retour de la fonction.

## Etape 6 : Ajout d'un nouvel hôtel

Enfin, dans ``AddPage.jsx``, créez dans le JSX un formulaire pour obtenir le nom, la description, la ville, l'adresse et le code postal d'un hôtel depuis l'utilisateur. Complétez la fonction handleSubmit et ajoutez-la comme event listener du formulaire. Faites un POST à l'API pour ajouter un hôtel.

